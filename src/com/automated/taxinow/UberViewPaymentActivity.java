/**
 * 
 */
package com.automated.taxinow;

import java.util.ArrayList;
import java.util.HashMap;

import com.automated.taxinow.adapter.PaymentListAdapter;
import com.automated.taxinow.models.Card;
import com.automated.taxinow.parse.HttpRequester;
import com.automated.taxinow.parse.ParseContent;
import com.automated.taxinow.utils.AndyUtils;
import com.automated.taxinow.utils.Const;
import com.automated.taxinow.utils.PreferenceHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Hardik A Bhalodi
 * 
 */
public class UberViewPaymentActivity extends ActionBarBaseActivitiy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uberorg.ActionBarBaseActivitiy#onCreate(android.os.Bundle)
	 */
	private ListView listViewPayment;
	private PaymentListAdapter adapter;
	private ArrayList<Card> listCards;
	private int REQUEST_ADD_CARD = 1;
	private LinearLayout tvNoHistory;
	private TextView tvHeaderText;
	private View v;
	private ImageView btnAddNewPayment;
	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		context=this;
		setContentView(R.layout.activity_view_payment);

		setTitle(getString(R.string.text_cards));
		setIcon(R.drawable.back);
		setIconMenu(R.drawable.ic_payment);

		listViewPayment = (ListView) findViewById(R.id.listViewPayment);
		tvNoHistory = (LinearLayout) findViewById(R.id.llEmptyView);
		tvHeaderText = (TextView) findViewById(R.id.tvHeaderText);
		btnAddNewPayment = (ImageView) findViewById(R.id.btnAddNewPayment);
		btnAddNewPayment.setOnClickListener(this);
		v = findViewById(R.id.line);
		v.setVisibility(View.GONE);
		listCards = new ArrayList<Card>();
		// actionBar.setDisplayHomeAsUpEnabled(true);
		// actionBar.setHomeButtonEnabled(true);
		// actionBar.setTitle(getString(R.string.text_cards));
		adapter = new PaymentListAdapter(this, listCards);
		listViewPayment.setAdapter(adapter);

		listViewPayment
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int pos, long arg3) {
						final Card cardsingle = (Card) listViewPayment
								.getItemAtPosition(pos);
					
						new AlertDialog.Builder(context)
								.setTitle("Delete entry")
								.setMessage(
										"Are you sure you want to delete card *****"+cardsingle.getLastFour()
												+ "?")
								.setPositiveButton(android.R.string.yes,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {
												removecards(cardsingle
														.getId());
											}
										})
								.setNegativeButton(android.R.string.no,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {
												// do nothing
											}
										})

								.show();

						return true;
					}

				});

		getCards();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnActionNotification:
			onBackPressed();
			break;
		case R.id.btnAddNewPayment:
			startActivityForResult(new Intent(this,
					UberAddPaymentActivity.class), REQUEST_ADD_CARD);
			break;
		default:
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uberorg.ActionBarBaseActivitiy#isValidate()
	 */
	@Override
	protected boolean isValidate() {
		// TODO Auto-generated method stub
		return false;
	}

	private void removecards(int cardid) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.REMOVE_CARD);
		map.put(Const.Params.ID, new PreferenceHelper(this).getUserId());
		map.put(Const.Params.TOKEN,
				new PreferenceHelper(this).getSessionToken());
		map.put(Const.Params.CARD_ID, String.valueOf(cardid));
		
Log.d("hey", "in remove card");
		new HttpRequester(this, map, Const.ServiceCode.REMOVE_CARD, this);
	}

	private void getCards() {
		AndyUtils.showCustomProgressDialog(this,
				getString(R.string.progress_loading), false, null);
		Log.d("user details", "ID=  " + new PreferenceHelper(this).getUserId()
				+ "TOKEN=  " + new PreferenceHelper(this).getSessionToken());

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL,
				Const.ServiceType.GET_CARDS + Const.Params.ID + "="
						+ new PreferenceHelper(this).getUserId() + "&"
						+ Const.Params.TOKEN + "="
						+ new PreferenceHelper(this).getSessionToken());
		new HttpRequester(this, map, Const.ServiceCode.GET_CARDS, true, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uberorg.ActionBarBaseActivitiy#onTaskCompleted(java.lang.String,
	 * int)
	 */
	@Override
	public void onTaskCompleted(String response, int serviceCode) {
		// TODO Auto-generated method stub
		AndyUtils.removeCustomProgressDialog();
		Log.d("user detail", response);
		switch (serviceCode) {
		case Const.ServiceCode.GET_CARDS:
			Log.d("hey", "get cards"+response);
			if (new ParseContent(this).isSuccess(response)) {
				listCards.clear();
				new ParseContent(this).parseCards(response, listCards);
				if (listCards.size() > 0) {
					listViewPayment.setVisibility(View.VISIBLE);
					tvNoHistory.setVisibility(View.GONE);
					v.setVisibility(View.VISIBLE);
					tvHeaderText.setVisibility(View.VISIBLE);
					btnAddNewPayment.setImageResource(R.drawable.another_card);
				} 
				adapter.notifyDataSetChanged();
			}else {
				listViewPayment.setVisibility(View.GONE);
				tvNoHistory.setVisibility(View.VISIBLE);
				tvHeaderText.setVisibility(View.GONE);
				v.setVisibility(View.GONE);
				btnAddNewPayment.setImageResource(R.drawable.add_credit);
			}
			break;
			
		case Const.ServiceCode.REMOVE_CARD:
			Log.d("hey", response);
			Log.d("hey", String.valueOf(new ParseContent(this).isSuccess(response)));
			if(new ParseContent(this).isSuccess(response)){
				Log.d("hey", "hmmm");
				getCards();
				Toast.makeText(context, "Card removed successfully", Toast.LENGTH_LONG).show();
			}
			break;

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uberorg.ActionBarBaseActivitiy#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case Activity.RESULT_OK:
			getCards();
			break;

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
