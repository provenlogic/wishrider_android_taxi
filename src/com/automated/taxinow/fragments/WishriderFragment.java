package com.automated.taxinow.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.automated.taxinow.MainDrawerActivity;
import com.automated.taxinow.R;

public class WishriderFragment extends UberBaseFragmentRegister implements OnClickListener{

	private ImageView userPhoto;
	private Button send,buy;
	private Activity activity;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.wishriderfragment, container, false);
		userPhoto = (ImageView) root.findViewById(R.id.userphoto);
		send = (Button) root.findViewById(R.id.send);
		buy = (Button) root.findViewById(R.id.buy);
		send.setOnClickListener(this);
		buy.setOnClickListener(this);
		activity = getActivity();
		userPhoto.setImageResource(R.drawable.ic_launcher);
				return root;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.send:
			startActivity(new Intent(activity, MainDrawerActivity.class));
			activity.finish();
			break;
		case R.id.buy:
			startActivity(new Intent(activity, MainDrawerActivity.class));
			activity.finish();
			break;

		default:
			break;
		}
	}
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}
	@Override
	protected boolean isValidate() {
		// TODO Auto-generated method stub
		return false;
	}

}
