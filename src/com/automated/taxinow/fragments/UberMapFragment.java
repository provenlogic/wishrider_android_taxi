package com.automated.taxinow.fragments;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import android.app.AlertDialog;
import android.app.Dialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;

import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.automated.taxinow.MainDrawerActivity;
import com.automated.taxinow.MainDrawerActivity.OnBackClick;
import com.automated.taxinow.ProfileActivity;
import com.automated.taxinow.R;
import com.automated.taxinow.UberViewPaymentActivity;
import com.automated.taxinow.adapter.PlacesAutoCompleteAdapter;
import com.automated.taxinow.adapter.VehicalTypeListAdapter;
import com.automated.taxinow.component.MyFontButton;
import com.automated.taxinow.component.MyFontEdittextView;
import com.automated.taxinow.component.MyFontHistoryTextView;
import com.automated.taxinow.component.MyFontTextView;
import com.automated.taxinow.component.MyTitleFontTextView;

import com.automated.taxinow.interfaces.OnProgressCancelListener;
import com.automated.taxinow.models.Card;
import com.automated.taxinow.models.Driver;
import com.automated.taxinow.models.VehicalType;

import com.automated.taxinow.models.Walkerinfo;

import com.automated.taxinow.parse.GMapV2Direction;
import com.automated.taxinow.parse.GetDirectionsAsyncTask;
import com.automated.taxinow.parse.HttpRequester;
import com.automated.taxinow.parse.ParseContent;
import com.automated.taxinow.utils.AndyUtils;
import com.automated.taxinow.utils.AppLog;
import com.automated.taxinow.utils.Const;
import com.automated.taxinow.utils.PreferenceHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

/**
 * @author Hardik A Bhalodi
 */

public class UberMapFragment extends UberBaseFragment implements
		OnProgressCancelListener,OnBackClick {

	private PlacesAutoCompleteAdapter adapter;
	private static AutoCompleteTextView enterdestination;

	public static boolean isMapTouched = false;
	private float currentZoom = -1;

	private GoogleMap map;
	private LocationClient client;
	private LatLng curretLatLng;
	private String strAddress = null;

	private boolean isContinueRequest;
	private Timer timer;
	private WalkerStatusReceiver walkerReceiver;

	private ImageButton btnMyLocation;

	private FrameLayout mapFrameLayout;

	private GridView listViewType;

	private ArrayList<VehicalType> listType;

	private ArrayList<Marker> drivermarkers;

	private VehicalTypeListAdapter typeAdapter;

	private int selectedPostion = -1;
	private boolean isGettingVehicalType = true;

	private boolean isLocationFound;

	// private Animation topToBottomAnimation, bottomToTopAnimation,
	// buttonTopToBottomAnimation;

	private MyFontButton btnSelectService, btnconfirmservice, btnpayment,
			btnratecard, btnfareestimate, btnpromocard;
	private static MyFontButton bubble;
	private static SlidingDrawer drawer;
	private static LinearLayout markers;
	private static RelativeLayout pickuppop;
	private static ImageButton btnadddestination;
	int payment_type = -1;// no payment selected
	private String payment_mode[] = { "By Card", "By Cash", "By PayPal" };
	private ArrayList<Walkerinfo> walkerlist;
	private TextView eta;
	private RelativeLayout destaddlayout;
	private ImageButton clearfield;
	private SharedPreferences promopref;
	SharedPreferences.Editor editorpromo;
	private String promoglobal;
	int paydebt_indicator = 0;
	private Animation slidedown, slideup;
	private LatLng destlatlng_places;

	private static Button setlocation;
	// PopupWindow window;

	//private static int backnext;
	private static Dialog dialog;
	private static Dialog storeLocation,storeLocation1;
	private static boolean setlocationtrue;
	private static Button send,buy;
	private static RelativeLayout whishrider,whishlocation,
	whish_reuest,wishridcomplete,removeview,whish;
	private static MyFontButton requestcomplete;
	private static ImageView userphoto,cross,driverImg;
	private static LatLng startPosition,endPosition;
	private static LatLngBounds latlngBounds;
	private static boolean afterpoly;
	private List<LatLng> listPosition =new ArrayList<LatLng>();
	private static Polyline newPolyline;
	 private static Marker m,m1;
	private static LayoutInflater li;
	private  TextView  driverName;
	private Button cancel,done;
	private TextView address;
	 String selectedDestPlace;
	
	private MyFontTextView distance,time;
	public static UberMapFragment newInstance() {
		UberMapFragment mapFragment = new UberMapFragment();
		return mapFragment;
	}

	TextView markerBubblePickMeUp;

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_map, container, false);
		isLocationFound = false;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li = LayoutInflater.from(activity);
		markerBubblePickMeUp = (TextView) view
				.findViewById(R.id.markerBubblePickMeUp);
		markerBubblePickMeUp.setOnClickListener(this);
		distance = (MyFontTextView) view.findViewById(R.id.distance);
		time = (MyFontTextView) view.findViewById(R.id.time);
		cross = (ImageView) view.findViewById(R.id.cross);
		cross.setOnClickListener(this);
		address = (TextView) view.findViewById(R.id.finallocation);
		userphoto = (ImageView) view.findViewById(R.id.userphoto);
		userphoto.setOnClickListener(this);
		send = (Button) view.findViewById(R.id.send);
		buy = (Button) view.findViewById(R.id.buy);
		cancel = (Button) view.findViewById(R.id.cancel);
		done = (Button) view.findViewById(R.id.done);
		send.setOnClickListener(this);
		buy.setOnClickListener(this);
		whishrider = (RelativeLayout) view.findViewById(R.id.wishrider);
		whishlocation = (RelativeLayout) view.findViewById(R.id.whish_location);
		whish = (RelativeLayout) view.findViewById(R.id.whish);
		setlocation = (Button) view.findViewById(R.id.setlocation);
	//	textlocation = (MyTitleFontTextView) view.findViewById(R.id.textlocation);
		setlocation.setOnClickListener(this);
		requestcomplete =  (MyFontButton) view.findViewById(R.id.requestcomplete);
		requestcomplete.setOnClickListener(this);
		removeview = (RelativeLayout) view.findViewById(R.id.removeview);
		driverImg = (ImageView) view.findViewById(R.id.driverImg);
		driverName = (TextView) view.findViewById(R.id.driverName);
		
		bubble = (MyFontButton) view.findViewById(R.id.markerBubblePickMeUp);
		selectedPostion = 0;/* modified by Amal *//* reverted back */

		listViewType = (GridView) view.findViewById(R.id.gvTypes);

		promopref = getActivity().getSharedPreferences("promocode",
				Context.MODE_PRIVATE);
		editorpromo = promopref.edit();
		
		markers = (LinearLayout) view.findViewById(R.id.layoutMarker);
		pickuppop = (RelativeLayout) view.findViewById(R.id.pickuppop);

		
		// llBottomLayout = (LinearLayout)
		// view.findViewById(R.id.llBottomLayout);
		mapFrameLayout = (FrameLayout) view.findViewById(R.id.mapFrameLayout);

		mapFrameLayout.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN | MotionEvent.ACTION_MOVE:
					UberMapFragment.isMapTouched = true;
					break;

				case MotionEvent.ACTION_UP:
					UberMapFragment.isMapTouched = false;
					break;
				}
				return true;

			}
		});
		btnMyLocation = (ImageButton) view.findViewById(R.id.btnMyLocation);
		btnSelectService = (MyFontButton) view
				.findViewById(R.id.btnSelectService);
		btnSelectService.setOnClickListener(this);

		btnconfirmservice = (MyFontButton) view
				.findViewById(R.id.btnconfirmservice);
		btnconfirmservice.setOnClickListener(this);
		btnpayment = (MyFontButton) view.findViewById(R.id.btnpayment);
		btnpayment.setOnClickListener(this);
		enterdestination = (AutoCompleteTextView) view
				.findViewById(R.id.EnterDestination);
		destaddlayout = (RelativeLayout) view
				.findViewById(R.id.destinationaddlayout);
		clearfield = (ImageButton) view.findViewById(R.id.clearfield);
		clearfield.setOnClickListener(this);

		btnfareestimate = (MyFontButton) view
				.findViewById(R.id.btnfareestimate);
		btnfareestimate.setOnClickListener(this);
		eta = (TextView) view.findViewById(R.id.eta);
		btnratecard = (MyFontButton) view.findViewById(R.id.btnratecard);
		btnratecard.setOnClickListener(this);
		btnpromocard = (MyFontButton) view.findViewById(R.id.btnpromocode);
		btnpromocard.setOnClickListener(this);
		slidedown = AnimationUtils.loadAnimation(getActivity(),
				R.anim.destaddtopbottom);
		slideup = AnimationUtils.loadAnimation(getActivity(),
				R.anim.destaddbottomtop);
		// etSource = (AutoCompleteTextView)
		// view.findViewById(R.id.etEnterSouce);
		drawer = (SlidingDrawer) view.findViewById(R.id.drawer);
		setUpMapIfNeeded();
		
		whish_reuest = (RelativeLayout) view.findViewById(R.id.wishriderreuest);
		wishridcomplete = (RelativeLayout) view.findViewById(R.id.wishridcomplete);
		//prepare dialog for driver info
		dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.car_details);
		storeLocation = new Dialog(activity);
		storeLocation.requestWindowFeature(Window.FEATURE_NO_TITLE);
		storeLocation1 = new Dialog(activity);
		storeLocation1.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if(activity == null){
			activity = (MainDrawerActivity) getActivity();
		}
		drivermarkers = new ArrayList<Marker>();

		btnadddestination = activity.btnadddestination;
		btnadddestination.setOnClickListener(this);

		 
		activity.tvTitle.setVisibility(View.GONE);
		IntentFilter filter = new IntentFilter(Const.INTENT_WALKER_STATUS);
		walkerReceiver = new WalkerStatusReceiver();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				walkerReceiver, filter);

		walkerlist = new ArrayList<Walkerinfo>();
		walkerarrayformarker = new ArrayList<UberMapFragment.walkerinfo_marker>();
		
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		adapter = new PlacesAutoCompleteAdapter(activity,
				R.layout.autocomplete_list_text);
		//etSource.setAdapter(adapter);

		enterdestination.setAdapter(adapter);
		enterdestination.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				hideKeyboard();
			}
		});

		// PopUp Window
		// LayoutInflater inflate = LayoutInflater.from(activity);
		// LinearLayout ll=(LinearLayout) inflate.inflate(R.layout.popup_window,
		// null);

		// window = new
		// PopupWindow(ll,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		// window.showAsDropDown(activity.btnNotification);

	
		listType = new ArrayList<VehicalType>();
		typeAdapter = new VehicalTypeListAdapter(activity, listType, this);
		listViewType.setAdapter(typeAdapter);
		getVehicalTypes();
		// drawer.lock();
		listViewType.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				for (int i = 0; i < listType.size(); i++)
					listType.get(i).isSelected = false;
				listType.get(position).isSelected = true;
				// btnSelectService.setCompoundDrawables(new , top, right,
				// bottom)
				// onItemClick(position);
				selectedPostion = position;

				removemarkers();
				getallproviders();

				typeAdapter.notifyDataSetChanged();
				/* added by amal */
				if (drawer.isOpened()) {
					drawer.animateClose();
					drawer.unlock();
				}

			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		activity.tvTitle.setVisibility(View.GONE);
//		activity.btnNotification.setVisibility(View.INVISIBLE);
//		etSource.setVisibility(View.VISIBLE);
//		startCheckingStatusUpdate();

	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (map == null) {
			map = ((SupportMapFragment) activity.getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			map.setMyLocationEnabled(true);
			map.getUiSettings().setMyLocationButtonEnabled(false);
			map.getUiSettings().setZoomControlsEnabled(false);
			map.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {

				@Override
				public void onMyLocationChange(Location loc) {
					// TODO Auto-generated method stub

				}
			});

			btnMyLocation.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Location loc = map.getMyLocation();
					if (loc != null) {
						LatLng latLang = new LatLng(loc.getLatitude(), loc
								.getLongitude());

						animateCameraToMarker(latLang);
						getallproviders();
					}

				}
			});

			map.setOnCameraChangeListener(new OnCameraChangeListener() {

				public void onCameraChange(CameraPosition camPos) {
					// TODO Auto-generated method stub
					if (currentZoom == -1) {
						currentZoom = camPos.zoom;
					} else if (camPos.zoom != currentZoom) {
						currentZoom = camPos.zoom;
						return;
					}

					if (!isMapTouched) {
						curretLatLng = camPos.target;
						// removemarkers();
						getallproviders();
						// if (pickuppop.getVisibility() == View.VISIBLE)
						// gettime();
						//getAddressFromLocation(camPos.target, );
						
					}
					isMapTouched = false;
					// setMarker(camPos.target);
				}
			});

			if (map != null) {
				// Log.i("Map", "Map Fragment");
			}
		}

		client = new LocationClient(activity, new ConnectionCallbacks() {

			@Override
			public void onDisconnected() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onConnected(Bundle connectionHint) {
				// TODO Auto-generated method stub

				Location loc = client.getLastLocation();

				if (loc != null) {

					LatLng latLang = new LatLng(loc.getLatitude(),
							loc.getLongitude());
					animateCameraToMarker(latLang);
				} else {
					activity.showLocationOffDialog();
				}

			}
		}, new OnConnectionFailedListener() {

			@Override
			public void onConnectionFailed(ConnectionResult result) {
				// TODO Auto-generated method stub

			}
		});
		client.connect();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		stopCheckingStatusUpdate();
		if (dialog != null) {
			dialog.dismiss();
		}
		super.onPause();

	}

	@Override
	public void onDestroyView() {

		SupportMapFragment f = (SupportMapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		if (f != null) {
			try {
				getFragmentManager().beginTransaction().remove(f).commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		map = null;
		if(storeLocation!=null){
			storeLocation.dismiss();
		}
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
				walkerReceiver);
		activity.tvTitle.setVisibility(View.VISIBLE);
		//etSource.setVisibility(View.GONE);
	}

	public static void setmarkerVisibile() {
		// markers.setVisibility(View.VISIBLE);
		bubble.setVisibility(View.VISIBLE);
		drawer.setVisibility(View.VISIBLE);
	}

	public static void setmarkerInvisibile() {
		// markers.setVisibility(View.INVISIBLE);
		bubble.setVisibility(View.INVISIBLE);
		drawer.setVisibility(View.INVISIBLE);
	}

	public static void setpickpopupVisible() {
		MainDrawerActivity.popon = true;
		pickuppop.setVisibility(View.VISIBLE);
		btnadddestination.setVisibility(View.VISIBLE);

	}

	public static void setpickpopupInvisible() {
		MainDrawerActivity.popon = false;
		pickuppop.setVisibility(View.INVISIBLE);
		btnadddestination.setVisibility(View.GONE);
	}
	private LatLngBounds createLatLngBoundsObject(LatLng firstLocation, LatLng secondLocation)
	{
		if (firstLocation != null && secondLocation != null)
		{
			LatLngBounds.Builder builder = new LatLngBounds.Builder();    
			builder.include(firstLocation).include(secondLocation);
			
			return builder.build();
		}
		return null;
	}
	private void ShowManage(){
		View promptsView = li.inflate(R.layout.wishrider_manageitem, null);
		storeLocation1.setContentView(promptsView);
		ImageView close = (ImageView) promptsView.findViewById(R.id.addclose);
		ImageView next = (ImageView) promptsView.findViewById(R.id.addnext);
		storeLocation1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		storeLocation1.setCancelable(false);
		// create alert dialog
//		 WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//		    lp.copyFrom(storeLocation1.getWindow().getAttributes());
//		    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//		    lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		    
			// show it
		    storeLocation1.show();
		  //  storeLocation1.getWindow().setAttributes(lp);

		    storeLocation1.setOnKeyListener(new Dialog.OnKeyListener() {

	            @Override
	            public boolean onKey(DialogInterface arg0, int keyCode,
	                    KeyEvent event) {
	                // TODO Auto-generated method stub
	                if (keyCode == KeyEvent.KEYCODE_BACK) {
	                	//Toast.makeText(activity, "back", 3).show();
	                	whish_reuest.setVisibility(View.GONE);
	    				whishlocation.setVisibility(View.VISIBLE);
	    				userphoto.setVisibility(View.VISIBLE);
	    				cross.setVisibility(View.VISIBLE);
	    				storeLocation1.dismiss();
	    				hideKeyboard();
	    				afterpoly = false;
	                }
	                return true;
	            }
	        });
		close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				whish_reuest.setVisibility(View.GONE);
				whishlocation.setVisibility(View.VISIBLE);
				userphoto.setVisibility(View.VISIBLE);
				cross.setVisibility(View.VISIBLE);
				storeLocation1.dismiss();
				hideKeyboard();
				afterpoly = false;
			}
		});
		next.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				whish_reuest.setVisibility(View.VISIBLE);
				whishlocation.setVisibility(View.GONE);
				  BitmapDescriptor a 
				   = BitmapDescriptorFactory.defaultMarker(
				     BitmapDescriptorFactory.HUE_GREEN);
				  BitmapDescriptor b 
				   = BitmapDescriptorFactory.defaultMarker(
				     BitmapDescriptorFactory.HUE_RED);
				  if(startPosition !=null && endPosition !=null){
				  m= map
							.addMarker(new MarkerOptions()
									.position(
											startPosition)
									.icon(a));
				  m1 = map
							.addMarker(new MarkerOptions()
									.position(
											endPosition)
									.icon(b));
				Map<String, String> map = new HashMap<String, String>();
				map.put(GetDirectionsAsyncTask.USER_CURRENT_LAT, String.valueOf(startPosition.latitude));
				map.put(GetDirectionsAsyncTask.USER_CURRENT_LONG, String.valueOf(startPosition.longitude));
				map.put(GetDirectionsAsyncTask.DESTINATION_LAT, String.valueOf(endPosition.latitude));
				map.put(GetDirectionsAsyncTask.DESTINATION_LONG, String.valueOf(endPosition.longitude));
				map.put(GetDirectionsAsyncTask.DIRECTIONS_MODE, GMapV2Direction.MODE_DRIVING);
				new GetDirectionsAsyncTask(activity, UberMapFragment.this).execute(map);
				storeLocation1.dismiss();
				hideKeyboard();
				  }else{
					  Toast.makeText(activity, "Invalied Location", 3).show();
				  }
			}
		});
	}
	public  void handleGetDirectionsResult(ArrayList<LatLng> directionPoints,Document doc) {
		PolylineOptions rectLine = new PolylineOptions().width(7).color(Color.parseColor("#6BABFB"));
		
		for(int i = 0 ; i < directionPoints.size() ; i++) 
		{          
			rectLine.add(directionPoints.get(i));
		}
		if (newPolyline != null)
		{
			newPolyline.remove();
		}
		newPolyline = map.addPolyline(rectLine);
		
		
			latlngBounds = createLatLngBoundsObject(startPosition, endPosition);
			CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(startPosition, 13);
			map.animateCamera(cu);
			GMapV2Direction object=new GMapV2Direction();
			//as
			Location loc1 = new Location("");
			loc1.setLatitude(startPosition.latitude);
			loc1.setLongitude(startPosition.longitude);

			Location loc2 = new Location("");
			loc2.setLatitude(endPosition.latitude);
			loc2.setLongitude(endPosition.longitude);
			
			float distanceInMeters = loc1.distanceTo(loc2);
			distance.setText("In "+new DecimalFormat("##.##").format((distanceInMeters/1000))+ "Km");
			time.setText("In "+object.getDurationText(doc));
			Log.e("", ""+object.getDistanceText(doc));
			markers.setVisibility(View.GONE);
			afterpoly = true;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buy:
			userphoto.setImageResource(R.drawable.filter);
			cross.setVisibility(View.VISIBLE);
			whishrider.setVisibility(View.GONE);
			whishlocation.setVisibility(View.VISIBLE);
			markers.setVisibility(View.VISIBLE);
			break;
		case R.id.send:
			userphoto.setImageResource(R.drawable.filter);
			cross.setVisibility(View.VISIBLE);
			whishrider.setVisibility(View.GONE);
			whishlocation.setVisibility(View.VISIBLE);
			markers.setVisibility(View.VISIBLE);
			break;	
		case R.id.requestcomplete:
			requestcomplete.setText("CREATING REQUEST");
			removeview.removeAllViews();
			pickMeUp();
			//new wishridercomplete().execute();
			break;
		case R.id.setlocation:
			userphoto.setVisibility(View.GONE);
			cross.setVisibility(View.GONE);
			storeLocation.setOnKeyListener(new Dialog.OnKeyListener() {

	            @Override
	            public boolean onKey(DialogInterface arg0, int keyCode,
	                    KeyEvent event) {
	                // TODO Auto-generated method stub
	                if (keyCode == KeyEvent.KEYCODE_BACK) {
	                	//Toast.makeText(activity, "back", 3).show();
	                	cross.setVisibility(View.VISIBLE);
						userphoto.setVisibility(View.VISIBLE);
	        			hideKeyboard();
	                    storeLocation.dismiss();
	                }
	                return true;
	            }
	        });
			if(setlocationtrue){
				View promptsView = li.inflate(R.layout.wishrider_delivary, null);
				storeLocation.setContentView(promptsView);
				//TextView address = (TextView) promptsView.findViewById(R.id.deliveryaddress);
				address.setText("");
				ImageView close = (ImageView) promptsView.findViewById(R.id.dclose);
				ImageView next = (ImageView) promptsView.findViewById(R.id.dnext);
				final AutoCompleteTextView etSource = (AutoCompleteTextView) promptsView.findViewById(R.id.search);
				//backnext = true;
				// create alert dialog
//				 WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//				    lp.copyFrom(storeLocation.getWindow().getAttributes());
//				    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//				    lp.height = WindowManager.LayoutParams.MATCH_PARENT;
				    
					// show it
				    storeLocation.show();
				    
					adapter = new PlacesAutoCompleteAdapter(activity,
							R.layout.autocomplete_list_text);
				//	etSource = activity.etSource;
					etSource.setAdapter(adapter);
					
					if(selectedDestPlace!=null){
						etSource.setText(""+selectedDestPlace);
					}
					final Drawable x = getResources().getDrawable(R.drawable.closecircled);//your x image, this one from standard android images looks pretty good actually
					x.setBounds(0, 0, 40, 40);
					etSource.setCompoundDrawables(null, null, x, null);
					etSource.addTextChangedListener(new TextWatcher() {
						
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {
							// TODO Auto-generated method stub

							adapter.getFilter().filter(s);
							adapter.notifyDataSetChanged();
						}
						
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count,
								int after) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void afterTextChanged(Editable s) {
							// TODO Auto-generated method stub
							
						}
					});
					etSource.setOnTouchListener(new OnTouchListener() {
						
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub
							 if (etSource.getCompoundDrawables()[2] == null) {
						            return false;
						        }
						        if (event.getAction() != MotionEvent.ACTION_UP) {
						            return false;
						        }
						        if (event.getX() > etSource.getWidth() - etSource.getPaddingRight() - x.getIntrinsicWidth()) {
						        	etSource.setText("");
						        	//etSource.setCompoundDrawables(null, null, null, null);
						        }
						        return false;
						}
					});
					etSource.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
								long arg3) {
							// TODO Auto-generated method stub
							hideKeyboard();
							final String selectedDestPlace = adapter.getItem(arg2);
							new Thread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
//									final LatLng latlng = getLocationFromAddress(selectedDestPlace);
									getlocfromaddressfromGoogleApi(selectedDestPlace);
//									if (latlng != null) {

										getActivity().runOnUiThread(new Runnable() {
			
											@Override
											public void run() {
												address.setText(""+selectedDestPlace);
									
			
											}
										});
			
//									}

								}
							}).start();

						}
					});
				    
				 //   storeLocation.getWindow().setAttributes(lp);
					 close.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								userphoto.setVisibility(View.VISIBLE);
								cross.setVisibility(View.VISIBLE);
								whish.setVisibility(View.VISIBLE);
								if(selectedDestPlace == null){
									selectedDestPlace = address.getText().toString();
								}
								hideKeyboard();
								storeLocation.dismiss();
							}
						});
						next.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								if(startPosition !=null){
								whishlocation.setVisibility(View.GONE);
								userphoto.setVisibility(View.VISIBLE);
								cross.setVisibility(View.VISIBLE);
								storeLocation.dismiss();
								hideKeyboard();
								userphoto.setVisibility(View.GONE);
								cross.setVisibility(View.GONE);
								address.setText(""+etSource.getText());
								ShowManage();
								}else{
									Toast.makeText(activity, "Invalid  Delivery Location", 3).show();
								}
							}
						});
			}else{
				
			
			View promptsView = li.inflate(R.layout.wishrider_setlocation, null);
		
			
			storeLocation.setContentView(promptsView);
			storeLocation.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			storeLocation.setCancelable(false);
			storeLocation.show();
			ImageView close = (ImageView) promptsView.findViewById(R.id.close);
			ImageView next = (ImageView) promptsView.findViewById(R.id.next);
			final AutoCompleteTextView etSource = (AutoCompleteTextView) promptsView.findViewById(R.id.search);
			final EditText setname = (EditText) promptsView.findViewById(R.id.setname);
			
			setname.setOnFocusChangeListener(new OnFocusChangeListener() {
				
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					// TODO Auto-generated method stub
					hideKeyboard();
				}
			});
//			View view = storeLocation.getCurrentFocus();
//			if (view != null) {  
//			InputMethodManager imm = (InputMethodManager)activity.getSystemService(
//			      Context.INPUT_METHOD_SERVICE);
//			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//			}
		
			adapter = new PlacesAutoCompleteAdapter(activity,
					R.layout.autocomplete_list_text);
		//	etSource = activity.etSource;
			etSource.setAdapter(adapter);

			if(selectedDestPlace!=null){
				etSource.setText(""+selectedDestPlace);
			}
			final Drawable x = getResources().getDrawable(R.drawable.closecircled);//your x image, this one from standard android images looks pretty good actually
			x.setBounds(0, 0, 40, 40);
			etSource.setCompoundDrawables(null, null, x, null);
			etSource.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub

					adapter.getFilter().filter(s);
					adapter.notifyDataSetChanged();
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
			etSource.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					 if (etSource.getCompoundDrawables()[2] == null) {
				            return false;
				        }
				        if (event.getAction() != MotionEvent.ACTION_UP) {
				            return false;
				        }
				        if (event.getX() > etSource.getWidth() - etSource.getPaddingRight() - x.getIntrinsicWidth()) {
				        	etSource.setText("");
				        	//etSource.setCompoundDrawables(null, null, null, null);
				        }
				        return false;
				}
			});
			etSource.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					// TODO Auto-generated method stub
					hideKeyboard();
					selectedDestPlace = adapter.getItem(arg2);
					new Thread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
//							final LatLng latlng = getLocationFromAddress(selectedDestPlace);
							getlocfromaddressfromGoogleApi(selectedDestPlace);
//							if (latlng != null) {

								getActivity().runOnUiThread(new Runnable() {
	
									@Override
									public void run() {
										// TODO Auto-generated method stub
										address.setText(""+selectedDestPlace);
	
									}
								});
	
//							}

						}
					}).start();

				}
			});
			
		 close.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					cross.setVisibility(View.VISIBLE);
					userphoto.setVisibility(View.VISIBLE);
					storeLocation.dismiss();
					whish.setVisibility(View.VISIBLE);
					activity.getWindow().setSoftInputMode(
						    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
						);
				}
			});

		 next.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					activity.getWindow().setSoftInputMode(
						    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
						);
					setname.clearFocus();
					if(startPosition !=null){
					cross.setVisibility(View.VISIBLE);
					userphoto.setVisibility(View.VISIBLE);
					userphoto.setImageResource(R.drawable.social_user);
					etSource.setText("");
//					textlocation.setText("Delivery Location");
					setlocation.setText("SET DELIVERY LOCATION");
					userphoto.setImageResource(R.drawable.social_user);
					setlocationtrue = true;
					address .setText(""+etSource.getText());
					whish.setVisibility(View.GONE);
					selectedDestPlace = null;
					storeLocation.dismiss();
					}
					else
						Toast.makeText(activity, "Invalid Store Location", 3).show();
					
				}
			});
			}
		 break;
		 
		case R.id.userphoto:
//			startActivity(new Intent(activity,
//					MainDrawerActivity.class));
			MainDrawerActivity.mMenuDrawer.toggleMenu();
			break;
		case R.id.cross:
			userphoto.setImageResource(R.drawable.social_user);
			cross.setVisibility(View.GONE);
			whishrider.setVisibility(View.VISIBLE);
			whishlocation.setVisibility(View.GONE);
			markers.setVisibility(View.GONE);
			if(afterpoly){
				whish_reuest.setVisibility(View.GONE);
				View promptsView = li.inflate(R.layout.wishrider_manageitem, null);
				storeLocation.setContentView(promptsView);
				ImageView close = (ImageView) promptsView.findViewById(R.id.addclose);
				ImageView next = (ImageView) promptsView.findViewById(R.id.addnext);
			
				storeLocation.show();
				close.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						userphoto.setImageResource(R.drawable.filter);
						cross.setVisibility(View.VISIBLE);
						whishrider.setVisibility(View.GONE);
						whishlocation.setVisibility(View.VISIBLE);
						setlocationtrue = false;
						setlocation.setText("SET STORE LOCATION");
						if(m != null && m1 != null && newPolyline != null){
							newPolyline.remove();
							m.remove();
							m1.remove();
						}
						afterpoly = false;
						storeLocation.dismiss();
						hideKeyboard();
					}
				});
				next.setOnClickListener(new OnClickListener() {
					
					@SuppressWarnings("unchecked")
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						whish_reuest.setVisibility(View.VISIBLE);
						whishlocation.setVisibility(View.GONE);
					//	backnext = false;
						Map<String, String> map = new HashMap<String, String>();
						map.put(GetDirectionsAsyncTask.USER_CURRENT_LAT, String.valueOf(startPosition.latitude));
						map.put(GetDirectionsAsyncTask.USER_CURRENT_LONG, String.valueOf(startPosition.longitude));
						map.put(GetDirectionsAsyncTask.DESTINATION_LAT, String.valueOf(endPosition.latitude));
						map.put(GetDirectionsAsyncTask.DESTINATION_LONG, String.valueOf(endPosition.longitude));
						map.put(GetDirectionsAsyncTask.DIRECTIONS_MODE, com.automated.taxinow.parse.GMapV2Direction.MODE_DRIVING);
						new GetDirectionsAsyncTask(activity, UberMapFragment.this).execute(map);
						storeLocation.dismiss();
						hideKeyboard();
					}
				});
			}
			break;
		case R.id.markerBubblePickMeUp:

			if (isValidate()) {
				setmarkerInvisibile();
				setpickpopupVisible();
				gettime();

				try {
					btnconfirmservice.setText("Request "
							+ listType.get(selectedPostion).getName());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			// getCards(); modified by amal
			/*
			 * if (isValidate()) { pickMeUp(); //modified by amal }
			 */

			break;
		case R.id.btnSelectService:
			if (drawer.isOpened()) {
				drawer.animateClose();
				drawer.unlock();
			} else {
				drawer.animateOpen();
				drawer.lock();
			}
			break;

		case R.id.btnconfirmservice:

			Log.d("amal", Integer.toString(payment_type));
			btnconfirmservice.setEnabled(false);
			btnconfirmservice.postDelayed(new Runnable() {
				@Override
				public void run() {
					btnconfirmservice.setEnabled(true);
				}
			}, 3000);

			if (payment_type == -1) {
				new AlertDialog.Builder(getActivity())
						.setTitle("No Payment option selected")
						.setMessage(
								"Please select any given payment option to request a ride")
						.setPositiveButton(android.R.string.ok,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// continue with delete
									}
								}).setIcon(android.R.drawable.ic_dialog_alert)
						.show();
			}
			if (payment_type == 0)
				getCards();
			if (payment_type == 1 || payment_type == 2) {
				Log.d("amal", "going to pick up");
				pickMeUp();
			}
			break;
		case R.id.btnpayment:
			selectPayment();
			break;
		case R.id.btnAdddestination:

			destaddlayout.setVisibility(View.VISIBLE);
			destaddlayout.startAnimation(slidedown);
			break;
		case R.id.btnfareestimate:
			if (enterdestination.getVisibility() == View.VISIBLE
					&& !TextUtils
							.isEmpty(enterdestination.getText().toString())) {
				getDistance();
			} else if (destaddlayout.getVisibility() == View.VISIBLE
					&& TextUtils.isEmpty(enterdestination.getText().toString())) {
				Toast.makeText(getActivity(), "Enter Destination",
						Toast.LENGTH_LONG).show();

			} else {
				destaddlayout.setVisibility(View.VISIBLE);
				destaddlayout.startAnimation(slidedown);
				Toast.makeText(getActivity(), "Enter Destination",
						Toast.LENGTH_LONG).show();
			}

			break;
		case R.id.btnratecard:
			// showpopup();
			/*
			 * final Dialog mDialog = new Dialog(getActivity(),
			 * R.style.MyFareestimateDialog);
			 * 
			 * // mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			 * mDialog.getWindow().setBackgroundDrawable( new
			 * ColorDrawable(android.graphics.Color.TRANSPARENT));
			 * mDialog.setContentView(R.layout.ratecard);
			 * mDialog.setTitle("Rate Card");
			 */

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflate = getActivity().getLayoutInflater();
			View contentview = inflate.inflate(R.layout.ratecard, null);
			View titleview = inflate
					.inflate(R.layout.ratecardcustomtitle, null);
			builder.setView(contentview).setCustomTitle(titleview);
			final AlertDialog mDialog = builder.create();

			MyFontTextView ratecardtitle;
			TextView baseprice,
			distanceprice,
			timeprice;
			baseprice = (TextView) contentview
					.findViewById(R.id.Basepricefield);
			distanceprice = (TextView) contentview
					.findViewById(R.id.Distancepricefield);
			timeprice = (TextView) contentview
					.findViewById(R.id.Timepricefield);
			ratecardtitle = (MyFontTextView) titleview
					.findViewById(R.id.ratecardtitle);

			baseprice.setText(listType.get(selectedPostion).getCurrency() + " "
					+ listType.get(selectedPostion).getBasePrice());
			distanceprice.setText(listType.get(selectedPostion).getCurrency()
					+ " "
					+ listType.get(selectedPostion).getPricePerUnitDistance()
					+ "/" + listType.get(selectedPostion).getUnit());
			timeprice.setText(listType.get(selectedPostion).getCurrency() + " "
					+ listType.get(selectedPostion).getPricePerUnitTime()
					+ "/min");
			ratecardtitle.setText(listType.get(selectedPostion).getName());
			mDialog.show();
			Log.d("amal", Integer.toString(selectedPostion));
			break;
		case R.id.btnpromocode:
			ApplyPromo();
			break;

		case R.id.clearfield:
			if (TextUtils.isEmpty(enterdestination.getText().toString())) {

				destaddlayout.startAnimation(slideup);
				destaddlayout.setVisibility(View.GONE);

			}
			if (!TextUtils.isEmpty(enterdestination.getText().toString()))
				enterdestination.setText("");

			break;
		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	private void showOptions() {
		// TODO Auto-generated method stub
		Log.d("pavan", "in show option");
		listViewType.setVisibility(View.GONE);
		drawer.setVisibility(View.GONE);

	}

	private void getAddressFromLocation(final LatLng latlng, final EditText et) {

		et.setTextColor(Color.GRAY);
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				Geocoder gCoder = new Geocoder(getActivity());
				try {
					final List<Address> list = gCoder.getFromLocation(
							latlng.latitude, latlng.longitude, 1);
					if (list != null && list.size() > 0) {
						Address address = list.get(0);
						StringBuilder sb = new StringBuilder();
						if (address.getAddressLine(0) != null) {

							sb.append(address.getAddressLine(0)).append(", ");
						}
						sb.append(address.getLocality()).append(", ");
						// sb.append(address.getPostalCode()).append(",");
						sb.append(address.getCountryName());
						strAddress = sb.toString();

						strAddress = strAddress.replace(",null", "");
						strAddress = strAddress.replace("null", "");
						strAddress = strAddress.replace("Unnamed", "");
					}
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							if (!TextUtils.isEmpty(strAddress)) {
								et.setFocusable(false);
								et.setFocusableInTouchMode(false);
								et.setText(strAddress);
								et.setTextColor(getResources().getColor(
										android.R.color.black));
								et.setFocusable(true);
								et.setFocusableInTouchMode(true);

							} else {
								et.setTextColor(getResources().getColor(
										android.R.color.black));
							}
						}
					});

				} catch (IOException exc) {
					exc.printStackTrace();
					getAddressFromGooleApi(latlng);
				}
			}

		}).start();

	}

	private void animateCameraToMarker(LatLng latLng) {
	
		CameraUpdate cameraUpdate = null;

		cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
		map.animateCamera(cameraUpdate);

	}

	private LatLng getLocationFromAddress(final String place) {
		Log.d("amal", place);
		LatLng loc = null;
		Geocoder gCoder = new Geocoder(getActivity());
		try {
			final List<Address> list = gCoder.getFromLocationName(place, 5);
			Log.e("amal", ""+list.size());
			// TODO Auto-generated method stub
			if (list != null && list.size() > 0) {

				loc = new LatLng(list.get(0).getLatitude(), list.get(0)
						.getLongitude());

			}
			
			getlocfromaddressfromGoogleApi(place);
			
		} catch (IOException e) {
			getlocfromaddressfromGoogleApi(place);
			if (destlatlng_places != null)
				loc = destlatlng_places;
			e.printStackTrace();
		}

		return loc;
	}

	@Override
	public void onProgressCancel() {
		stopCheckingStatusUpdate();
		cancleRequest();

		// stopCheckingStatusUpdate();
	}

	private void getAddressFromGooleApi(LatLng latlong) {

		// call google api here
		AndyUtils.removeCustomProgressDialog();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.GOOGLE_LOCATION + latlong.latitude
				+ "," + latlong.longitude);
		AppLog.Log("pavan", Const.URL);
		new HttpRequester(activity, map, Const.ServiceCode.GET_ADDRESS, true,
				this);
	}

	private void getlocfromaddressfromGoogleApi(String address) {
		
		String encode = "";
		try {
			encode = URLEncoder.encode(address, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			Toast.makeText(activity, "Encoding Faild", 3).show();
			e.printStackTrace();
		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.GOOGLE_ADDRESS + encode);
		AppLog.Log("pavan", Const.URL);
		new HttpRequester(activity, map, Const.ServiceCode.GET_LOCATION, true,
				this);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uberorg.fragments.BaseFragment#isValidate()
	 */
	@Override
	protected boolean isValidate() {
		// TODO Auto-generated method stub
		String msg = null;
		if (curretLatLng == null) {
			msg = getString(R.string.text_location_not_found);
		} else if (selectedPostion == -1) {
			msg = getString(R.string.text_select_type);
		}//		} else if (TextUtils.isEmpty(etSource.getText().toString())
//				|| etSource.getText().toString()
//						.equalsIgnoreCase("Waiting for Address")) {
//			msg = getString(R.string.text_waiting_for_address);
//		}
		if (msg == null)
			return true;
		Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
		return false;
	}

	private void paydebt() {
		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(getResources().getString(R.string.no_internet),
					activity);
			return;
		}
		AndyUtils.showCustomProgressDialog(activity,
				getString(R.string.text_creating_request), true, null);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.PAY_DEBT);
		map.put(Const.Params.TOKEN,
				new PreferenceHelper(activity).getSessionToken());
		map.put(Const.Params.ID, new PreferenceHelper(activity).getUserId());
		new HttpRequester(activity, map, Const.ServiceCode.PAY_DEBT, this);

	}

	private void pickMeUp() {
		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(getResources().getString(R.string.no_internet),
					activity);
			return;
		}
		AndyUtils.showCustomProgressDialog(activity,
				getString(R.string.text_creating_request), true, null);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.CREATE_REQUEST);
		map.put(Const.Params.TOKEN,
				new PreferenceHelper(activity).getSessionToken());
		map.put(Const.Params.ID, new PreferenceHelper(activity).getUserId());
		map.put(Const.Params.LATITUDE, String.valueOf(startPosition.latitude));
		map.put(Const.Params.LONGITUDE, String.valueOf(startPosition.longitude));
		map.put(Const.Params.TYPE,
				String.valueOf(listType.get(selectedPostion).getId()));
//		if (enterdestination.getVisibility() == View.VISIBLE
//				&& !TextUtils.isEmpty(enterdestination.getText().toString())) {
//			LatLng destlatlng = getLocationFromAddress(enterdestination
//					.getText().toString());
//			if (destlatlng != null) {
//				
//			} else {
//				AndyUtils.removeCustomProgressDialog();
//				Toast.makeText(activity, "Please Enter The Address Correctly",
//						Toast.LENGTH_LONG).show();
//				return;
//			}
//		}
		map.put(Const.Params.DEST_LATITUDE,
				String.valueOf(endPosition.latitude));
		map.put(Const.Params.DEST_LONGITUDE,
				String.valueOf(endPosition.longitude));
		if (promopref.getString("promocode", "") != "") {
			map.put(Const.Params.PROMO_CODE,
					promopref.getString("promocode", ""));
		}
		map.put(Const.Params.COD, String.valueOf(payment_type));
		map.put(Const.Params.DISTANCE, "1");
		Log.d("xxx", "map " + map.toString());
		new HttpRequester(activity, map, Const.ServiceCode.CREATE_REQUEST, this);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uberorg.fragments.BaseFragment#onTaskCompleted(java.lang.String,
	 * int)
	 */
	@Override
	public void onTaskCompleted(String response, int serviceCode) {
		// TODO Auto-generated method stub
		try {
		super.onTaskCompleted(response, serviceCode);
		
		String distance = "";
		String duration = "";
		// AndyUtils.removeCustomProgressDialog();
		
		switch (serviceCode) {
		case Const.ServiceCode.CREATE_REQUEST:
			Log.d("amal", ""+response);
			AndyUtils.removeCustomProgressDialog();
			if (activity.pContent.isSuccess(response)) {
				editorpromo.putString("promocode", "");
				editorpromo.commit();
				// AndyUtils.removeCustomProgressDialog();
				activity.pHelper.putRequestId(activity.pContent
						.getRequestId(response));
				AndyUtils.showCustomProgressDialog(activity,
						getString(R.string.text_contacting), false, this);
				startCheckingStatusUpdate();
			} else {
				if (activity.pContent.getErrorCode(response) == 417) {
					String errormsg = "";
					try {
						JSONObject obj = new JSONObject(response);
						errormsg = obj.getString("error");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					AlertDialog.Builder debtalert = new AlertDialog.Builder(
							activity);
					debtalert
							.setTitle("Could not request ride")
							.setMessage(errormsg)
							.setPositiveButton("Pay",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub
											paydebt_indicator = 1;
											getCards();

										}

									});
					debtalert.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

								}
							});
					debtalert.show();

				}
			}
			break;
		case Const.ServiceCode.GET_REQUEST_STATUS:

			if (activity.pContent.isSuccess(response)) {
				switch (activity.pContent.checkRequestStatus(response)) {
				case Const.IS_WALK_STARTED:
				case Const.IS_WALKER_ARRIVED:
				case Const.IS_COMPLETED:
				case Const.IS_WALKER_STARTED:
					AndyUtils.removeCustomProgressDialog();
					stopCheckingStatusUpdate();
					Driver driver = activity.pContent.getDriverDetail(response);
//					if (btnadddestination.getVisibility() == View.VISIBLE) {
//						btnadddestination.setVisibility(View.GONE);
//					}
					//activity.gotoTripFragment(driver);
					whish_reuest.setVisibility(View.GONE);
					wishridcomplete.setVisibility(View.VISIBLE);
					Picasso.with(activity)
					.load(driver.getPicture())
					.placeholder(R.drawable.ic_launcher)
					.into(driverImg);
					//driverbill.setText(""+driver.getBill());
					cancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
						}
					});
					done.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							wishridcomplete.setVisibility(View.GONE);
							whishrider.setVisibility(View.VISIBLE);
						}
					});
					driverName.setText(""+driver.getFirstName()+" "+driver.getLastName());
					break;
				case Const.IS_WALKER_RATED:
					stopCheckingStatusUpdate();
					activity.gotoRateFragment(activity.pContent
							.getDriverDetail(response));
					break;

				case Const.IS_REQEUST_CREATED:
					if (activity.pHelper.getRequestId() != Const.NO_REQUEST)
						AndyUtils.showCustomProgressDialog(activity,
								getString(R.string.text_contacting), false,
								this);
					isContinueRequest = true;
					break;
				case Const.NO_REQUEST:
					if (!isGettingVehicalType) {
						AndyUtils.removeCustomProgressDialog();
					}
					stopCheckingStatusUpdate();
					break;
				default:
					isContinueRequest = false;
					break;
				}
				Log.d("mahi", "response" + response);
			} else if (activity.pContent.getErrorCode(response) == Const.REQUEST_ID_NOT_FOUND) {
				AndyUtils.removeCustomProgressDialog();
				activity.pHelper.clearRequestData();
				isContinueRequest = false;
			} else if (activity.pContent.getErrorCode(response) == Const.REQUEST_CANCEL) {
				AndyUtils.removeCustomProgressDialog();
				activity.pHelper.clearRequestData();
				isContinueRequest = false;
			}

			else if (activity.pContent.getErrorCode(response) == Const.INVALID_TOKEN) {
				if (activity.pHelper.getLoginBy()
						.equalsIgnoreCase(Const.MANUAL))
					login();
				else
					loginSocial(activity.pHelper.getUserId(),
							activity.pHelper.getLoginBy());
			}

			else {
				isContinueRequest = true;
			}
			break;
		case Const.ServiceCode.LOGIN:
			if (activity.pContent.isSuccessWithStoreId(response)) {

			}
			break;
		case Const.ServiceCode.CANCEL_REQUEST:

			Log.d("mahi", "response in cancel request" + response);

			if (activity.pContent.isSuccess(response)) {

			}
			activity.pHelper.clearRequestData();
			AndyUtils.removeCustomProgressDialog();
			break;
		case Const.ServiceCode.GET_VEHICAL_TYPES:

			if (activity.pContent.isSuccess(response)) {
				listType.clear();
				activity.pContent.parseTypes(response, listType);
				if (listType.size() > 0) {
					if (listType != null && listType.get(0) != null)
						listType.get(0).isSelected = true;
					typeAdapter.notifyDataSetChanged();
				}
			}
			AndyUtils.removeCustomProgressDialog();
			break;
		/* added by amal */
		case Const.ServiceCode.GET_CARDS:

			Log.d("amal", "GET CARD" + response);

			if (new ParseContent(getActivity()).isSuccess(response)) {
				ArrayList<Card> listCards;
				listCards = new ArrayList<Card>();
				listCards.clear();
				new ParseContent(getActivity()).parseCards(response, listCards);
				if (listCards.size() > 0) {

					if (paydebt_indicator == 1) {
						paydebt();
						paydebt_indicator = 0;
					} else
						pickMeUp();

				}
				adapter.notifyDataSetChanged();
			} else {
				Log.d("yyy", "in else of 0 card");
				startActivity(new Intent(getActivity(),
						UberViewPaymentActivity.class));

			}

			AndyUtils.removeCustomProgressDialog();
			break;

		case Const.ServiceCode.PAY_DEBT:
			Log.d("amal", "pay debt" + response);
			if (activity.pContent.isSuccess(response)) {
				Toast.makeText(activity, "Debt cleared successfully",
						Toast.LENGTH_LONG).show();
			} else
				Toast.makeText(activity, "Debt not cleared successfully",
						Toast.LENGTH_LONG).show();
			AndyUtils.removeCustomProgressDialog();
			break;
		case Const.ServiceCode.PAYMENT_OPTIONS:
			Log.d("amal", ""+response);
			if (activity.pContent.isSuccess(response)) {
				List<String> paymentoption = new ArrayList<String>();
				paymentoption.clear();
				// String[] paymentoptions=new String[2];
				int inter = 0;
				try {
					JSONObject jsonobject = new JSONObject(response);
					JSONObject obj = jsonobject
							.getJSONObject("payment_options");
					if (obj.getInt("stored_cards") == 1) {
						paymentoption.add("By Card");
						/*
						 * paymentoptions[inter]="By Card"; inter++;
						 */
					}
					if (obj.getInt("cod") == 1) {
						paymentoption.add("By Cash");
						/*
						 * paymentoptions[inter]="By Cash"; inter++;
						 */
					}
					if (obj.getInt("paypal") == 1) {
						paymentoption.add("By PayPal");
						/*
						 * paymentoptions[inter]="By PayPal"; inter++;
						 */
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				final CharSequence[] paymentoptions = paymentoption
						.toArray(new CharSequence[paymentoption.size()]);

				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Pay");

				builder.setItems(paymentoptions,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface optiondialog,
									int which) {
								if (paymentoptions[which].toString().equals(
										payment_mode[0])) {

									payment_type = 0;
									btnpayment.setText("Pay by Card");
								} else if (paymentoptions[which].toString()
										.equals(payment_mode[1])) {

									payment_type = 1;
									btnpayment.setText("Pay by Cash");

								} else if (paymentoptions[which].toString()
										.equals(payment_mode[2])) {

									payment_type = 2;
									btnpayment.setText("Pay by PayPal");
								}

							}
						});
				builder.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								arg0.dismiss();
							}
						});
				AlertDialog alert = builder.create();
				alert.setCancelable(true);
				alert.show();

			}
			AndyUtils.removeCustomProgressDialog();
			break;
		case Const.ServiceCode.GET_MAP_DETAILS:
			AndyUtils.removeCustomProgressDialog();
			Log.d("amal", "in distance success" + response);
			if (response != null) {

				try {
					JSONObject jObject = new JSONObject(response);

					if (jObject.getString("status").equals("OK")) {

						JSONArray jaArray = jObject.getJSONArray("rows");

						for (int i = 0; i < jaArray.length(); i++) {

							JSONObject jobj = jaArray.getJSONObject(i);

							JSONArray jaArray2 = jobj.getJSONArray("elements");

							for (int j = 0; i < jaArray2.length(); j++) {

								JSONObject jobj1 = jaArray2.getJSONObject(j);

								JSONObject jobj_distance = jobj1
										.getJSONObject("distance");
								Log.d("amal",
										"distance "
												+ jobj_distance
														.getString("text")
												+ " , "
												+ jobj_distance
														.getString("value"));

								JSONObject jobj_duration = jobj1
										.getJSONObject("duration");
								Log.d("amal",
										"distance "
												+ jobj_duration
														.getString("text")
												+ " , "
												+ jobj_duration
														.getString("value"));

								duration = jobj_duration.getString("value");
								distance = jobj_distance.getString("value");
								getEstimation(distance, duration);

							}

						}

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			break;
		case Const.ServiceCode.GET_MAP_TIME:
			if (response != null) {

				try {
					JSONObject jObject = new JSONObject(response);

					if (jObject.getString("status").equals("OK")) {

						JSONArray jaArray = jObject.getJSONArray("rows");

						for (int i = 0; i < jaArray.length(); i++) {

							JSONObject jobj = jaArray.getJSONObject(i);

							JSONArray jaArray2 = jobj.getJSONArray("elements");

							for (int j = 0; i < jaArray2.length(); j++) {

								JSONObject jobj1 = jaArray2.getJSONObject(j);

								JSONObject jobj_distance = jobj1
										.getJSONObject("distance");

								JSONObject jobj_duration = jobj1
										.getJSONObject("duration");

								duration = jobj_duration.getString("text");
								eta.setText("Pick up time is approximately "
										+ duration);

							}

						}

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			AndyUtils.removeCustomProgressDialog();
			break;

		case Const.ServiceCode.FARE_CALCULATOR:
			AndyUtils.removeCustomProgressDialog();
			Log.d("amal", "in farecalc success" + response);
			if (response != null) {
				try {
					JSONObject jObject = new JSONObject(response);
					if (jObject.getString("success").equals("true")) {

						ShowFare(jObject.getString("estimated_fare"),
								jObject.getString("currency"));

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			break;
		case Const.ServiceCode.GETPROVIDER_ALL:
			Log.d("amal", ""+response);
			if (activity.pContent.isSuccess(response)) {
				walkerlist.clear();
				try {
					JSONObject jsonobject = new JSONObject(response);
					JSONArray jsonarr = jsonobject.getJSONArray("walkers");
					for (int i = 0; i < jsonarr.length(); i++) {
						JSONObject obj = jsonarr.getJSONObject(i);
						final Walkerinfo walkerinfo = new Walkerinfo();
						walkerinfo.setId(obj.getString("id"));
						walkerinfo.setLatitude(obj.getString("latitude"));
						walkerinfo.setLongitude(obj.getString("longitude"));
						walkerinfo.setTime_cost(obj.getString("time_cost"));
						walkerinfo.setDistance_cost(obj
								.getString("distance_cost"));
						walkerinfo.setType(obj.getString("type"));
						walkerinfo.setDistance(obj.getString("distance"));
						walkerinfo.setBase_price(obj.getString("base_price"));
						Log.d("amal", obj.getString("base_price"));
						walkerlist.add(walkerinfo);

						int flag = 0;
						if (walkerarrayformarker.size() == 0) {
							Marker m = map
									.addMarker(new MarkerOptions()
											.position(
													new LatLng(
															Double.parseDouble(walkerinfo
																	.getLatitude()),
															Double.parseDouble(walkerinfo
																	.getLongitude())))
											.icon(BitmapDescriptorFactory
													.fromResource(R.drawable.bike)));
							walkerinfo_marker mwalkerinfo_marker = new walkerinfo_marker(
									walkerinfo, m);
							walkerarrayformarker.add(mwalkerinfo_marker);
						}

						for (int k = 0; k < walkerarrayformarker.size(); k++) {
							Log.d("hey", "going to for loop");

							Log.d("hey",
									String.valueOf(walkerarrayformarker.get(k)
											.getWalkerlistclone().getId()));
							Log.d("hey", String.valueOf(walkerinfo.getId()));
							if (TextUtils.equals(
									String.valueOf(walkerarrayformarker.get(k)
											.getWalkerlistclone().getId()),
									String.valueOf(walkerinfo.getId()))) {
								flag++;
								if (walkerarrayformarker.get(k)
										.getWalkerlistclone().getLatitude() != walkerinfo
										.getLatitude()
										|| walkerarrayformarker.get(k)
												.getWalkerlistclone()
												.getLongitude() != walkerinfo
												.getLongitude()) {

									walkerarrayformarker.get(k)
											.getDrivermarkers().remove();
									walkerarrayformarker.get(k)
											.setWalkerlistclone(walkerinfo);
									Marker m = map
											.addMarker(new MarkerOptions()
													.position(
															new LatLng(
																	Double.parseDouble(walkerinfo
																			.getLatitude()),
																	Double.parseDouble(walkerinfo
																			.getLongitude())))
													.icon(BitmapDescriptorFactory
															.fromResource(R.drawable.bike)));
									walkerarrayformarker.get(k)
											.setDrivermarkers(m);

								}
							}
						}
						if (flag == 0) {
							Log.d("hey", "in flag");
							final Marker m = map
									.addMarker(new MarkerOptions()
											.position(
													new LatLng(
															Double.parseDouble(walkerinfo
																	.getLatitude()),
															Double.parseDouble(walkerinfo
																	.getLongitude())))
											.icon(BitmapDescriptorFactory
													.fromResource(R.drawable.pin_driver_car)));
							final walkerinfo_marker mwalkerinfo_marker = new walkerinfo_marker(
									walkerinfo, m);
							walkerarrayformarker.add(mwalkerinfo_marker);

						}

					}

					for (int s = 0; s < walkerarrayformarker.size(); s++) {
						int flag1 = 0;
						for (int r = 0; r < walkerlist.size(); r++) {
							if (TextUtils.equals(walkerarrayformarker.get(s)
									.getWalkerlistclone().getId(), walkerlist
									.get(r).getId())) {
								flag1++;
							}

						}
						if (flag1 == 0) {
							walkerarrayformarker.get(s).getDrivermarkers()
									.remove();
							walkerarrayformarker.remove(s);
						}

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// markthewalkers();

			} else {
				removemarkers();
				walkerlist.clear();

			}
			if (pickuppop.getVisibility() == View.VISIBLE) {
				gettime();
			}
//			map.setOnMarkerClickListener(new OnMarkerClickListener() {
//
//				@Override
//				public boolean onMarkerClick(Marker arg0) {
//					// TODO Auto-generated method stub
//
//					DecimalFormat format = new DecimalFormat("#.########");
//					for (int i = 0; i < walkerlist.size(); i++) {
//						Walkerinfo info = walkerlist.get(i);
//						String curr = format.format(arg0.getPosition().longitude);
//						if (info.getLongitude().startsWith(curr)) {
//							VehicalType currentType = listType.get(i);
//							// custom dialog
//							// set the custom dialog components - text, image
//							// and button
//							MyFontTextView carName = (MyFontTextView) dialog
//									.findViewById(R.id.myFontTextView_car_name);
//							carName.setText("" + currentType.getName());
//							ImageView image = (ImageView) dialog
//									.findViewById(R.id.imageView_service_icon);
//							image.setImageResource(R.drawable.ic_launcher);
//							if (currentType.getIcon() != null
//									&& !currentType.getIcon().equals("")) {
//								Picasso.with(activity)
//										.load(currentType.getIcon())
//										.placeholder(R.drawable.ic_launcher)
//										.into(image);
//							} else {
//								Toast.makeText(
//										activity,
//										"Image URL is empty."
//												+ currentType.getIcon(), 3)
//										.show();
//
//							}
//							MyFontHistoryTextView pertime = (MyFontHistoryTextView) dialog
//									.findViewById(R.id.per_min_rate);
//							MyFontHistoryTextView perdis = (MyFontHistoryTextView) dialog
//									.findViewById(R.id.per_mile_rate);
//							MyFontHistoryTextView numseat = (MyFontHistoryTextView) dialog
//									.findViewById(R.id.myFontHistoryTextView_number_of_seat);
//							MyFontHistoryTextView avifare = (MyFontHistoryTextView) dialog
//									.findViewById(R.id.myFontHistoryTextView_min_fare);
//							Button dialogButton = (Button) dialog
//									.findViewById(R.id.button_close);
//
//							// maxseat.setText(""+currentType.g);
//							numseat.setText("" + currentType.getCurrency());
//							avifare.setText("" + currentType.getUnit());
//							perdis.setText(""
//									+ currentType.getPricePerUnitDistance());
//							pertime.setText(""
//									+ currentType.getPricePerUnitTime());
//
//							// if button is clicked, close the custom dialog
//							dialogButton
//									.setOnClickListener(new OnClickListener() {
//										@Override
//										public void onClick(View v) {
//											dialog.dismiss();
//										}
//									});
//
//							dialog.show();
//						}
//					}
//
//					return true;
//				}
//			});
			AndyUtils.removeCustomProgressDialog();
			break;

		case Const.ServiceCode.GET_PROMO_REQUEST:
			if (activity.pContent.isSuccess(response)) {
				String discount = "";

				try {
					JSONObject obj = new JSONObject(response);
					discount = obj.getString("discount");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				promoresponse.setText("You get " + discount
						+ " off in your current ride");
				editorpromo.putString("promocode", promoglobal);
				editorpromo.commit();
				Toast.makeText(activity, "PromoCode entered successfully",
						Toast.LENGTH_LONG).show();

			}

			AndyUtils.removeCustomProgressDialog();
			break;

		case Const.ServiceCode.GET_ADDRESS:

			AndyUtils.removeCustomProgressDialog();

			try {
				Log.d("pavan", "map response" + response);
				JSONObject jobj = new JSONObject(response);

				if (jobj.get("status").equals("OK")) {

					Log.d("pavan", "in okay");

					JSONArray jarray = jobj.getJSONArray("results");

					if (jarray.length() > 0) {

						JSONObject jobj1 = jarray.getJSONObject(0);

						strAddress = jobj1.getString("formatted_address");
						strAddress = strAddress.replace(",null", "");
						strAddress = strAddress.replace("null", "");
						strAddress = strAddress.replace("Unnamed", "");

						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								if (!TextUtils.isEmpty(strAddress)) {
//									etSource.setFocusable(false);
//									etSource.setFocusableInTouchMode(false);
//									etSource.setText(strAddress);
//									etSource.setTextColor(getResources()
//											.getColor(android.R.color.black));
//									etSource.setFocusable(true);
//									etSource.setFocusableInTouchMode(true);

								} else {
//									etSource.setText("");
//									etSource.setTextColor(getResources()
//											.getColor(android.R.color.black));
								}
							//	etSource.setEnabled(true);
							}
						});
					}

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;

		case Const.ServiceCode.GET_LOCATION:
			try {
				JSONObject jsonobject = new JSONObject(response);

				if (jsonobject.get("status").equals("OK")) {
					JSONArray jarray = jsonobject.getJSONArray("results");

					if (jarray.length() > 0) {

						JSONObject jobj1 = jarray.getJSONObject(0);
						JSONObject jobj2 = jobj1.getJSONObject("geometry");
						JSONObject jobj3 = jobj2.getJSONObject("location");
						listPosition.add(new LatLng(jobj3.getDouble("lat"),
								jobj3.getDouble("lng")));
						if(setlocationtrue){
							endPosition = new LatLng(jobj3.getDouble("lat"),
									jobj3.getDouble("lng"));
							animateCameraToMarker(endPosition);
							Log.e("", "endPosition"+endPosition.latitude);
						}else{
							startPosition = new LatLng(jobj3.getDouble("lat"),
									jobj3.getDouble("lng"));
							animateCameraToMarker(startPosition);
							Log.e("", "startPosition "+startPosition.longitude);
						}
						
						
//						destlatlng_places = new LatLng(jobj3.getDouble("lat"),
//								jobj3.getDouble("lng"));
						

					}

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		}
	}catch(Exception e){e.printStackTrace();}
	}

	private class TimerRequestStatus extends TimerTask {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (isContinueRequest) {
				isContinueRequest = false;
				getRequestStatus(String
						.valueOf(activity.pHelper.getRequestId()));
			}
		}

	}

	private void getRequestStatus(String requestId) {

		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.dialog_no_inter_message),
					activity);
			return;
		}

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL,
				Const.ServiceType.GET_REQUEST_STATUS + Const.Params.ID + "="
						+ new PreferenceHelper(activity).getUserId() + "&"
						+ Const.Params.TOKEN + "="
						+ new PreferenceHelper(activity).getSessionToken()
						+ "&" + Const.Params.REQUEST_ID + "=" + requestId);

		AppLog.Log("GET REQUEST STATUS",
				Const.ServiceType.GET_REQUEST_STATUS + Const.Params.ID + "="
						+ new PreferenceHelper(activity).getUserId() + "&"
						+ Const.Params.TOKEN + "="
						+ new PreferenceHelper(activity).getSessionToken()
						+ "&" + Const.Params.REQUEST_ID + "=" + requestId);

		new HttpRequester(activity, map, Const.ServiceCode.GET_REQUEST_STATUS,
				true, this);
	}

	private void startCheckingStatusUpdate() {
		stopCheckingStatusUpdate();
		if (activity.pHelper.getRequestId() != Const.NO_REQUEST) {
			isContinueRequest = true;
			timer = new Timer();
			timer.scheduleAtFixedRate(new TimerRequestStatus(), Const.DELAY,
					Const.TIME_SCHEDULE);
		}
	}

	private void stopCheckingStatusUpdate() {
		isContinueRequest = false;
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	private void cancleRequest() {
		Log.d("mahi", "in cancel request");
		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(getResources().getString(R.string.no_internet),
					activity);
			return;
		}
		AndyUtils.removeCustomProgressDialog();
		AndyUtils.showCustomProgressDialog(activity,
				getString(R.string.text_canceling_request), true, null);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.CANCEL_REQUEST);
		map.put(Const.Params.ID, String.valueOf(activity.pHelper.getUserId()));
		map.put(Const.Params.TOKEN,
				String.valueOf(activity.pHelper.getSessionToken()));
		map.put(Const.Params.REQUEST_ID,
				String.valueOf(activity.pHelper.getRequestId()));

		new HttpRequester(activity, map, Const.ServiceCode.CANCEL_REQUEST, this);
	}

	class WalkerStatusReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			String response = intent.getStringExtra(Const.EXTRA_WALKER_STATUS);
			if (TextUtils.isEmpty(response))
				return;
			stopCheckingStatusUpdate();

			if (activity.pContent.isSuccess(response)) {
				switch (activity.pContent.checkRequestStatus(response)) {
				case Const.IS_WALK_STARTED:
				case Const.IS_WALKER_ARRIVED:
				case Const.IS_COMPLETED:
				case Const.IS_WALKER_STARTED:
					AndyUtils.removeCustomProgressDialog();
					// stopCheckingStatusUpdate();
					Driver driver = activity.pContent.getDriverDetail(response);
//					Log.d("amal", "driver detail  --->" + driver.toString());
//					if (btnadddestination.getVisibility() == View.VISIBLE) {
//						btnadddestination.setVisibility(View.GONE);
//					}
					whish_reuest.setVisibility(View.GONE);
					wishridcomplete.setVisibility(View.VISIBLE);
					Picasso.with(activity)
					.load(driver.getPicture())
					.placeholder(R.drawable.ic_launcher)
					.into(driverImg);
					//driverbill.setText("");
					driverName.setText(""+driver.getFirstName()+" "+driver.getLastName());
//					removeThisFragment();
//
//					activity.gotoTripFragment(driver);
					
					break;
				case Const.IS_WALKER_RATED:
					// stopCheckingStatusUpdate();
					activity.gotoRateFragment(activity.pContent
							.getDriverDetail(response));
					break;

				case Const.IS_REQEUST_CREATED:
					AndyUtils.showCustomProgressDialog(activity,
							getString(R.string.text_contacting), false,
							UberMapFragment.this);
					startCheckingStatusUpdate();
					isContinueRequest = true;
					break;
				default:
					isContinueRequest = false;
					break;
				}

			} else if (activity.pContent.getErrorCode(response) == Const.REQUEST_ID_NOT_FOUND) {
				AndyUtils.removeCustomProgressDialog();
				activity.pHelper.clearRequestData();
				isContinueRequest = false;
			} else if (activity.pContent.getErrorCode(response) == Const.INVALID_TOKEN) {
				if (activity.pHelper.getLoginBy()
						.equalsIgnoreCase(Const.MANUAL))
					login();
				else
					loginSocial(activity.pHelper.getUserId(),
							activity.pHelper.getLoginBy());
			} else {
				isContinueRequest = true;
				startCheckingStatusUpdate();
			}
			// startCheckingStatusUpdate();

		}
	}

	private void removeThisFragment() {
		try {
			getActivity().getSupportFragmentManager().beginTransaction()
					.remove(this).commitAllowingStateLoss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getVehicalTypes() {
		isGettingVehicalType = true;
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.GET_VEHICAL_TYPES);
		AppLog.Log(Const.TAG, Const.URL);
		new HttpRequester(activity, map, Const.ServiceCode.GET_VEHICAL_TYPES,
				true, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget
	 * .AdapterView, android.view.View, int, long)
	 */
	public void onItemClick(int pos) {
		selectedPostion = pos;

	}

	/*
	 * Added by Amal
	 */
	private void getCards() {
		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.dialog_no_inter_message),
					activity);
			return;
		}

		AndyUtils.showCustomProgressDialog(getActivity(),
				getString(R.string.progress_loading), false, null);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.GET_CARDS + Const.Params.ID + "="
				+ new PreferenceHelper(getActivity()).getUserId() + "&"
				+ Const.Params.TOKEN + "="
				+ new PreferenceHelper(getActivity()).getSessionToken());
		new HttpRequester(getActivity(), map, Const.ServiceCode.GET_CARDS,
				true, this);
	}

	private void selectPayment() {
		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.dialog_no_inter_message),
					activity);
			return;
		}

		AndyUtils.showCustomProgressDialog(getActivity(),
				getString(R.string.progress_loading), false, null);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL,
				Const.ServiceType.PAYMENT_OPTIONS + Const.Params.ID + "="
						+ new PreferenceHelper(activity).getUserId() + "&"
						+ Const.Params.TOKEN + "="
						+ new PreferenceHelper(activity).getSessionToken());

		new HttpRequester(activity, map, Const.ServiceCode.PAYMENT_OPTIONS,
				true, this);

	}

	private void getDistance() {

		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.dialog_no_inter_message),
					activity);
			return;
		}

		AndyUtils.showCustomProgressDialog(getActivity(),
				getString(R.string.progress_loading), false, null);
		HashMap<String, String> map = new HashMap<String, String>();

		LatLng deslatlng = getLocationFromAddress(enterdestination.getText()
				.toString());
		if (deslatlng != null) {
			map.put(Const.URL,
					Const.ServiceType.GOOGLE_MAP_API + Const.Params.MAP_ORIGINS
							+ "=" + String.valueOf(curretLatLng.latitude) + ","
							+ String.valueOf(curretLatLng.longitude) + "&"
							+ Const.Params.MAP_DESTINATIONS + "="
							+ String.valueOf(deslatlng.latitude) + ","
							+ String.valueOf(deslatlng.longitude));

			Log.d("amal", "request from getdistance " + map);
			new HttpRequester(activity, map, Const.ServiceCode.GET_MAP_DETAILS,
					true, this);
		} else {
			AndyUtils.removeCustomProgressDialog();
			Toast.makeText(activity, "Please Enter The Address Correctly",
					Toast.LENGTH_LONG).show();
			return;
		}

	}

	TextView promoresponse;

	private void ApplyPromo() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflate = getActivity().getLayoutInflater();
		View contentview = inflate.inflate(R.layout.promocard, null);
		View titleview = inflate.inflate(R.layout.promocodecustomtitle, null);
		builder.setView(contentview).setCustomTitle(titleview);
		final AlertDialog mdialog = builder.create();
		mdialog.show();
		MyFontButton apply, cancel;

		final MyFontEdittextView promofield;
		apply = (MyFontButton) contentview.findViewById(R.id.promoApply);
		cancel = (MyFontButton) contentview.findViewById(R.id.promoCancel);
		promofield = (MyFontEdittextView) contentview.findViewById(R.id.promo);
		promoresponse = (TextView) contentview.findViewById(R.id.promoresponse);

		apply.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sendpromo(promofield.getText().toString());

			}
		});
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mdialog.dismiss();

			}
		});

	}

	void sendpromo(String promostring) {
		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.dialog_no_inter_message),
					activity);
			return;
		}

		promoglobal = promostring;
		AndyUtils.showCustomProgressDialog(getActivity(),
				getString(R.string.progress_loading), false, null);
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL,
				Const.ServiceType.GET_PROMO_REQUEST + Const.Params.ID + "="
						+ new PreferenceHelper(activity).getUserId() + "&"
						+ Const.Params.TOKEN + "="
						+ new PreferenceHelper(activity).getSessionToken()
						+ "&" + Const.Params.PROMO_CODE + "=" + promostring);

		new HttpRequester(activity, map, Const.ServiceCode.GET_PROMO_REQUEST,
				true, this);
	}

	private void gettime() {

		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.dialog_no_inter_message),
					activity);
			return;
		}

		if (walkerlist.isEmpty()) {
			eta.setText("No Providers Nearby");
		} else {

			HashMap<String, String> map = new HashMap<String, String>();
			map.put(Const.URL,
					Const.ServiceType.GOOGLE_MAP_API + Const.Params.MAP_ORIGINS
							+ "=" + String.valueOf(curretLatLng.latitude) + ","
							+ String.valueOf(curretLatLng.longitude) + "&"
							+ Const.Params.MAP_DESTINATIONS + "="
							+ walkerlist.get(0).getLatitude() + ","
							+ walkerlist.get(0).getLongitude());

			Log.d("amal", "request " + map);
			new HttpRequester(activity, map, Const.ServiceCode.GET_MAP_TIME,
					true, this);
		}
	}

	private void getEstimation(String distance, String duration) {
		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(
					getResources().getString(R.string.dialog_no_inter_message),
					activity);
			return;
		}

		AndyUtils.showCustomProgressDialog(getActivity(),
				getString(R.string.progress_loading), false, null);

		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Const.URL, Const.ServiceType.FARE_CALCULATOR);
		map.put(Const.Params.TOKEN,
				new PreferenceHelper(getActivity()).getSessionToken());
		map.put(Const.Params.ID,
				new PreferenceHelper(getActivity()).getUserId());
		map.put(Const.Params.TIME, String.valueOf(duration));
		map.put(Const.Params.DISTANCE, String.valueOf(distance));
		// map.put(Const.Params.TYPE,
		// String.valueOf(listType.get(selectedPostion).getId()));
		//
		// map.put(Const.Params.DISTANCE, "1");
		Log.d("amal", "request " + map);
		new HttpRequester(activity, map, Const.ServiceCode.FARE_CALCULATOR,
				this);

		// Log.d("amal", "request " + map);

	}

	private void ShowFare(String fare, String currency) {
		Log.d("amal", "in show fare");
		final Dialog mDialog = new Dialog(getActivity(),
				R.style.MyFareestimateDialog);

		mDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		mDialog.setContentView(R.layout.fareestimate);

		TextView fromaddress, toaddress, estimatedfare, servicename;
		MyFontButton fareclose;
		fromaddress = (TextView) mDialog.findViewById(R.id.fromaddress);
		toaddress = (TextView) mDialog.findViewById(R.id.toaddress);
		estimatedfare = (TextView) mDialog.findViewById(R.id.estimatedfare);
		servicename = (TextView) mDialog.findViewById(R.id.servicename);
		fareclose = (MyFontButton) mDialog.findViewById(R.id.faredialogclose);
		//fromaddress.setText(etSource.getText().toString());
		toaddress.setText(enterdestination.getText().toString());
		estimatedfare.setText(currency + " " + fare);
		servicename.setText(listType.get(selectedPostion).getName());
		fareclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mDialog.dismiss();

			}
		});
		mDialog.show();

	}

	private void getallproviders() {
		Log.d("amal", "in get all provider");
		if (!AndyUtils.isNetworkAvailable(activity)) {
			AndyUtils.showToast(getResources().getString(R.string.no_internet),
					activity);
			return;
		}

		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(Const.URL, Const.ServiceType.GETPROVIDER_ALL);
			map.put(Const.Params.TOKEN,
					new PreferenceHelper(activity).getSessionToken());
			map.put(Const.Params.ID, new PreferenceHelper(activity).getUserId());
			map.put(Const.Params.LATITUDE,
					String.valueOf(curretLatLng.latitude));
			map.put(Const.Params.LONGITUDE,
					String.valueOf(curretLatLng.longitude));
			map.put(Const.Params.TYPE,
					String.valueOf(listType.get(selectedPostion).getId()));
			map.put(Const.Params.DISTANCE, "1");
			new HttpRequester(activity, map, Const.ServiceCode.GETPROVIDER_ALL,
					this);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void removemarkers() {
		for (int a = 0; a < walkerarrayformarker.size(); a++) {
			walkerarrayformarker.get(a).getDrivermarkers().remove();
		}
		walkerarrayformarker.clear();
		/*
		 * for (int i = 0; i < drivermarkers.size(); i++) { if
		 * (drivermarkers.get(i) != null) drivermarkers.get(i).remove(); }
		 * drivermarkers.clear();
		 */
	}

	void markthewalkers() {

		for (int i = 0; i < walkerlist.size(); i++) {
			Marker m = map.addMarker(new MarkerOptions().position(
					new LatLng(Double.parseDouble(walkerlist.get(i)
							.getLatitude()), Double.parseDouble(walkerlist.get(
							i).getLongitude()))).icon(
					BitmapDescriptorFactory
							.fromResource(R.drawable.pin_driver_car)));
			drivermarkers.add(m);
		}

	}

	private class walkerinfo_marker {
		private Walkerinfo walkerlistclone;
		private Marker drivermarkers;

		public walkerinfo_marker(Walkerinfo walkerinfo, Marker m) {
			walkerlistclone = walkerinfo;
			drivermarkers = m;
		}

		public Walkerinfo getWalkerlistclone() {
			return walkerlistclone;
		}

		public void setWalkerlistclone(Walkerinfo walkerlistclone) {
			this.walkerlistclone = walkerlistclone;
		}

		public Marker getDrivermarkers() {
			return drivermarkers;
		}

		public void setDrivermarkers(Marker drivermarkers) {
			this.drivermarkers = drivermarkers;
		}

	}

	private void hideKeyboard() {
		// Check if no view has focus:
		View view = activity.getCurrentFocus();
		if (view != null) {
			InputMethodManager inputManager = (InputMethodManager) activity
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(view.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	private ArrayList<walkerinfo_marker> walkerarrayformarker;
	@Override
	public void OnBeckenable(final MainDrawerActivity activity) {
		// TODO Auto-generated method stub
		this.activity = activity;
		storeLocation.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                    KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                	//Toast.makeText(activity, "back", 3).show();
                	userphoto.setImageResource(R.drawable.filter);
                	userphoto.setVisibility(View.VISIBLE);
        			cross.setVisibility(View.VISIBLE);
        			whishrider.setVisibility(View.GONE);
        			whishlocation.setVisibility(View.VISIBLE);
        			setlocationtrue = false;
        			//etSource.setText("");
        			setlocation.setText("SET STORE LOCATION");
        			if(m != null && m1 != null && newPolyline != null){
        				newPolyline.remove();
        				m.remove();
        				m1.remove();
        			}
        			afterpoly = false;
        			storeLocation.dismiss();
        			hideKeyboard();
                    storeLocation.dismiss();
                }
                return true;
            }
        });
		
	
		if(afterpoly){
			whish_reuest.setVisibility(View.GONE);
			View promptsView = li.inflate(R.layout.wishrider_manageitem, null);
			storeLocation.setContentView(promptsView);
			ImageView close = (ImageView) promptsView.findViewById(R.id.addclose);
			ImageView next = (ImageView) promptsView.findViewById(R.id.addnext);
		//	backnext = 5;
			storeLocation.show();
			close.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					userphoto.setImageResource(R.drawable.filter);
					userphoto.setVisibility(View.VISIBLE);
					cross.setVisibility(View.VISIBLE);
					whishrider.setVisibility(View.GONE);
					whishlocation.setVisibility(View.VISIBLE);
					setlocationtrue = false;
					//etSource.setText("");
					setlocation.setText("SET STORE LOCATION");
					if(m != null && m1 != null && newPolyline != null){
						newPolyline.remove();
						m.remove();
						m1.remove();
					}
					afterpoly = false;
					storeLocation.dismiss();
					hideKeyboard();
				}
			});
			next.setOnClickListener(new OnClickListener() {
				
				@SuppressWarnings("unchecked")
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					whish_reuest.setVisibility(View.VISIBLE);
					whishlocation.setVisibility(View.GONE);

					Map<String, String> map = new HashMap<String, String>();
					map.put(GetDirectionsAsyncTask.USER_CURRENT_LAT, String.valueOf(startPosition.latitude));
					map.put(GetDirectionsAsyncTask.USER_CURRENT_LONG, String.valueOf(startPosition.longitude));
					map.put(GetDirectionsAsyncTask.DESTINATION_LAT, String.valueOf(endPosition.latitude));
					map.put(GetDirectionsAsyncTask.DESTINATION_LONG, String.valueOf(endPosition.longitude));
					map.put(GetDirectionsAsyncTask.DIRECTIONS_MODE, com.automated.taxinow.parse.GMapV2Direction.MODE_DRIVING);
					new GetDirectionsAsyncTask(UberMapFragment.this.activity, UberMapFragment.this).execute(map);
					storeLocation.dismiss();
					hideKeyboard();
				}
			});
		}
		
//		if(!afterpoly && !setlocationtrue && !backnext){
//			activity.finish();
//		}
	}
}
